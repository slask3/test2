#!/usr/bin/env python
import requests
import xml.etree.ElementTree as ET

def get_news_titles(rss_url, num_titles=10):
    response = requests.get(rss_url)

    if response.status_code == 200:
        # Parse XML content
        root = ET.fromstring(response.content)

        # Find all 'item' elements within the 'channel'
        items = root.findall(".//channel/item")

        # Extract titles of the first 'num_titles' items
        titles = [item.find("title").text for item in items[:num_titles]]

        return titles
    else:
        print(f"Error: {response.status_code}")
        return None

if __name__ == "__main__":
    rss_url = "https://rss.nytimes.com/services/xml/rss/nyt/Europe.xml"
    news_titles = get_news_titles(rss_url, num_titles=10)

    if news_titles:
        print("Titles of the first 10 news items:")
        for i, title in enumerate(news_titles, start=1):
            print(f"{i}. {title}")
    else:
        print("Unable to fetch news titles.")
